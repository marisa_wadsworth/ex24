﻿using System;

namespace ex24
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            /// Task A ///
            firstMethod("");
            firstMethod("");
            firstMethod("");
            firstMethod("");
            firstMethod("");
    
            Console.WriteLine("");

            /// Task B ///
            Name("Marisa");
            Name("Adrian");
            Name("Amindeep");
            Name("Summer");
            Name("Lucy");

            Console.WriteLine("");

            // Task C //
            Console.WriteLine(showName("Marisa"));
            Console.WriteLine(showName("Adrian"));
            Console.WriteLine(showName("Amindeep"));
            Console.WriteLine(showName("Summer"));
            Console.WriteLine(showName("Lucy"));

            Console.WriteLine("");

            // Task D //
            Console.WriteLine($"2 x 5 x 10 = {showNum(2,5,10)}");
            Console.WriteLine($"2 x 4 x 6 = {showNum(2,4,6)}");
            Console.WriteLine($"2 x 8 x 16 = {showNum(2,8,16)}");
            Console.WriteLine($"10 x 10 x 20 = {showNum(10,10,20)}");
            Console.WriteLine($"5 x 3 x 9 = {showNum(5,3,9)}");
            
            Console.WriteLine("");

            // Task E //
            myNumber(26);
        }
    static void firstMethod(string message)
    {
        Console.WriteLine($"My first method");
    }
    static void Name(string name)
    {
        Console.WriteLine($"Hello, {name} ");
    }
    static string showName(string name)
    {
        return $"My name is {name}";
    }
    static int showNum(int num1, int num2, int num3)
    {
        return num1*num2*num3;
    }
    static void myNumber(int a)
    {
        for(int b = 1; b<=a; b++)
        {
            Console.WriteLine(b);
        }
    }
}
}
